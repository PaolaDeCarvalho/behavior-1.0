<?php include('../sys/gerais.php'); ?>
<script>
    $(document).ready(function () {
        $('.voltar').removeClass('hidden');
    });
</script> 

<?php if (renovarProjeto() == 0) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                Nenhum projeto selecionado!
            </div>
        </div>
    </div>
<?php } else { ?>
    <form class="form-horizontal" method="post" action="sys/renovar.php">
        <fieldset>
            <legend>
                <?php print_r(renovarProjeto()['tituloProjeto']); ?><br/>
                <small class="label label-info"><?php print_r(renovarProjeto()['metaProjeto']); ?></small><br/>
                <small class="label label-success"><?php print_r(renovarProjeto()['recompensaProjeto']); ?></small>
            </legend>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Nova Data de Conclusão</label>
                    <input type="date" name="novaDataConclusao" class="form-control">
                </div>
            </div>

            
            <input type="hidden" id="idProjeto" name="idProjetoPontos" value="<?php print_r(renovarProjeto()['idProjeto']); ?>"/>
            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success right">Renovar</button>
                </div>
            </div>
        </fieldset>
    </form>

<?php } ?>


