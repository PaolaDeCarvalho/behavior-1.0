<?php
include('../sys/gerais.php');
foreach (todosProjetos() as $projeto) {
    
}
?>
<script>
    $(document).ready(function () {
        $('.voltar').addClass('hidden');
        $('.change-color').addClass('white');
    });
</script>
<section>
    <div class="row">
        <div class="col-lg-12">
            <?php if (isset($_GET['r']) != null) { ?>
                <div class="alert alert-<?php $_GET['r'] == 'true' ? 'success' : 'danger' ?>">
                    <?php print_r($_GET['r']); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<section class="row">
    <div class="col-lg-12">
        <?php
        
        if (mysqli_num_rows(todosProjetos()) == 0) {
            $resposta = 'Você não possui projetos ativos';
            ?>
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <strong>Ops!</strong> Você não possui projetos ativos no momento.
                </div>
            </div>
            <?php
        } else {
            foreach (todosProjetos() as $projeto) {
                ?>
                <div class="col-lg-4">
                    <div class="<?php print_r($projeto['corProjeto']); ?> projeto">
                        <div class="projeto-heading" href="#projeto">
                            <span><?php print_r($projeto['tituloProjeto']); ?></span>
                        </div>
                        <div class="projeto-meta">
                            <label>META</label>
                            <p><?php print_r($projeto['metaProjeto']); ?></p>
                        </div>
                        <div class="projeto-recompensa">
                            <label>RECOMPENSA</label>
                            <p><?php print_r($projeto['recompensaProjeto']); ?></p>
                        </div>

                        <?php
                        $pontos = PontosProjeto($projeto['idProjeto']);
                        if ($pontos == null) {
                            ?>
                            <div class="projeto-pontos">
                                <label>PONTOS</label><br/>
                                <span>Ainda não existem pontos cadastrados neste projeto</span>
                            </div>
                        <?php } else  { ?>
                            <div class="projeto-pontos">
                                <label>PONTOS</label><br/><br/>
                                <span class="text-right"><?php print_r($pontos['totalPontos']); ?></span>
                            </div>
                        <?php }
                            $dataP = date_create($pontos['diaPontos']);
                            if (date_format(date_create($projeto['dataPrevistaFinalProjeto']), 'Y-m-d')  == date("Y-m-d") || date_format(date_create($projeto['dataPrevistaFinalProjeto']), 'Y-m-d')  < date("Y-m-d")) {
                        ?> 
                        <span class="btn-group btn-group-justified">
                            <span class="btn btn-block" onclick="carregarPagina('renovar','?p=<?php print_r($projeto['idProjeto']); ?>')">
                                <span class="glyphicon glyphicon-repeat"></span>
                                Renovar Projeto
                            </span>
                            <span class="btn btn-block" onclick="carregarPagina('concluir','?p=<?php print_r($projeto['idProjeto']); ?>')">
                                <span class="glyphicon glyphicon-ok"></span>
                                Concluir Projeto
                            </span>
                        </span>
                        <?php } else if ($pontos != null && date_format($dataP,'Y-m-d') == date("Y-m-d"))  { ?>
                            <button data-toggle="tooltip" title="Você já incluiu os pontos de hoje" class="btn btn-block" disabled="true" onclick="carregarPagina('dados','?p=<?php print_r($projeto['idProjeto']); ?>')">
                                <span class="glyphicon glyphicon-calendar"></span>
                                Incluir dados de hoje 
                            </button>
                        <?php } else  { ?>
                            <button class="btn btn-block" onclick="carregarPagina('dados','?p=<?php print_r($projeto['idProjeto']); ?>')">
                                <span class="glyphicon glyphicon-calendar"></span>
                                Incluir dados de hoje
                            </button>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>
<!--<section class="row">
    <div class="col-lg-12 c-white">
        <div class="col-lg-3">
            <div class="panel sun-flower">
                <span class="glyphicon glyphicon-calendar icon"></span>
                Teste

                <div class="panel-footer text-center">
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel pomegranate">
                <span class="glyphicon glyphicon-cog icon"></span>
                Teste

                <div class="panel-footer text-center">
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel belize-hole">
                <span class="glyphicon glyphicon-comment icon"></span>
                Teste

                <div class="panel-footer text-center">
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel concrete">
                <span class="glyphicon glyphicon-tags icon"></span>
                Teste

                <div class="panel-footer text-center">
                </div>
            </div>
        </div>
    </div>
</section>-->


<?php
desconectarBanco();
?>