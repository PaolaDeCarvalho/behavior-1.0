<?php include('../sys/gerais.php'); ?>
<script>
    $(document).ready(function () {
        $('.voltar').removeClass('hidden');
    });
</script>

<?php if (incluirDados() == 0) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                Nenhum projeto selecionado!
            </div>
        </div>
    </div>
<?php } else { ?>
    <form class="form-horizontal" method="post" action="sys/pontos.php">
        <fieldset>
            <legend>
                <?php print_r(incluirDados()['tituloProjeto']); ?><br/>
                <small class="label label-info"><?php print_r(incluirDados()['metaProjeto']); ?></small><br/>
                <small class="label label-success"><?php print_r(incluirDados()['recompensaProjeto']); ?></small>
            </legend>
            <div class="col-lg-6">
                <table class="bonsHabitos table table-striped">
                    <thead>
                        <tr>
                            <th colspan="3" class="text-center bg-success">Bons Hábitos</th>
                        </tr>
                        <tr>
                            <th class="col-lg-8">Nível</th>
                            <th class="col-lg-3">Pontuação</th>
                            <th class="col-lg-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"><input type="number"  class="form-control parasomar" name="totalPontosBons" readonly="true" id="totalPontosBons"/></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-lg-6">
                <table class="ruinsHabitos table table-striped">
                    <thead>
                        <tr>
                            <th colspan="3" class="text-center bg-danger">Hábitos Ruins</th>
                        </tr>
                        <tr>
                            <th class="col-lg-8">Nível</th>
                            <th class="col-lg-3">Pontuação</th>
                            <th class="col-lg-1"></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"><input type="number" class="form-control parasomar" name="totalPontosRuins" id="totalPontosRuins" readonly="true"/></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="col-sm-12">
                <div class="form-group">
                    <span id="totalPontos"></span>
                    <input type="hidden" class="form-control" name="PontosTotais" id="PontosTotais" readonly="true"/>
                </div>
            </div>
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Elogio do Dia</label>
                    <input type="text" class="form-control" name="elogioDia" id="elogioDia" required="true"/>
                </div>
            </div>
            
            <input type="hidden" id="jsonNivelPontuacaoBom" name="jsonNivelPontuacaoBom"/>
            <input type="hidden" id="jsonNivelPontuacaoRuim" name="jsonNivelPontuacaoRuim"/>
            <input type="hidden" id="idProjetoPontos" name="idProjetoPontos" value="<?php print_r(incluirDados()['idProjeto']); ?>"/>
            <input type="hidden" value="<?php print_r(date("Y-m-d h:i:s")); ?>" name="date"/>
            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success right">Salvar</button>
                </div>
            </div>
        </fieldset>
    </form>

<?php } ?>

<script type="text/javascript">

    
    function preencheTabelaBonsHabitos(dataBom){
        var obj = JSON.parse(dataBom);
        for($i= 0; $i < obj.length; $i++){
            $html = '<tr><td>'+ obj[$i].nivel +'</td><td>'+ obj[$i].pontuacao +'</td><td><input type="checkbox" name="checkBom" class="checkBom" onchange="somarBons(this)"/></td></tr>';
            $('.bonsHabitos').find('tbody').prepend($html);
        }
        
    }
    $dataBom = '<?php print_r(incluirDados()['nivelPontuacaoBomProjeto']); ?>';
    preencheTabelaBonsHabitos($dataBom);
    
    function preencheTabelaRuinsHabitos(dataRuim){
        
        var obj = JSON.parse(dataRuim);
        for($i= 0; $i < obj.length; $i++){
            $html = '<tr><td>'+ obj[$i].nivel +'</td><td>'+ obj[$i].pontuacao +'</td><td><input type="checkbox" name="checkRuim" class="checkRuim" onchange="somarRuins(this)"/></td></tr>';
            $('.ruinsHabitos').find('tbody').prepend($html);
        }
        
    }
    $dataRuim = '<?php print_r(incluirDados()['nivelPontuacaoRuimProjeto']); ?>';
    console.log($dataRuim);
    preencheTabelaRuinsHabitos($dataRuim);
    
    var pontosBons = 0;
    function somarBons(check){
        var este = $(check).parent().parent();
        var ponto = (parseInt($(este).find(':nth-child(2)').html()));
        if(check.checked) {
            pontosBons += ponto;
        }
        else {
            pontosBons -= ponto;
        }
        
        
        $('#totalPontosBons').val(pontosBons);
    }
    
    var pontosRuins = 0;
    function somarRuins(check){
        var este = $(check).parent().parent();
        var ponto = (parseInt($(este).find(':nth-child(2)').html()));
        if(check.checked) {
            pontosRuins += ponto;
        }
        else {
            pontosRuins -= ponto;
        }
        
         $('#totalPontosRuins').val(pontosRuins);
    }
    
    $('input:checkbox').change(function(){
        var soma = 0;
        var pontosB = $('#totalPontosRuins').val();
        var pontosR = $('#totalPontosBons').val();
        console.log(pontosB + "," + pontosR);
        if(pontosB == "") {
            pontosB = 0;
        }
        else if(pontosR == "") {
            pontosR = 0;
        }
        soma =  parseInt(pontosB) + parseInt(pontosR);
        
        $('#totalPontos').children('#somaPontos').remove();
        $('#totalPontos').append('<span class="h1" id="somaPontos">'+ soma +'</span>');
        $('#PontosTotais').val(soma);
    });
    
</script>

