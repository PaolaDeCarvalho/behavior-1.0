<script>
    $(document).ready(function () {
        $('.voltar').removeClass('hidden');
        $('.change-color').removeClass('white');
    });
</script>
<form class="form-horizontal" method="post" action="sys/cadastro.php">
    <fieldset>
        <legend>Novo Projeto</legend>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="inputTitulo" class="">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="inputTitulo" required="true" placeholder="ex.: #Projeto 1">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-6">
                <label for="inputFim" class="">Fim</label><br/>
                <small>Data prevista para o Término</small>
                <input type="date" class="form-control" id="inputFim" name="inputFim" required="true" placeholder="Conclusão Prevista">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-6">
                <label for="inputCategoriaBoa" class="">Categoria Ruim</label><br/>
                <small>Escreva a categoria de hábito ruim que você deseja mudar</small>
                <input type="text" class="form-control" id="inputCategoriaBoa" name="inputCategoriaBoa" required="true" placeholder="ex.: Má Alimentação">
            </div>
            <div class="col-lg-6">
                <label for="inputCategoriaRuim" class="">Categoria Boa</label><br/>
                <small>Escreva a categoria de hábito bom que você deseja acrescentar</small>
                <input type="text" class="form-control" id="inputCategoriaRuim" name="inputCategoriaRuim" required="true" placeholder="ex.: Atividade Física"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="textMeta" class="">Meta</label><br/>
                <small>Descreva o objetivo deste projeto</small>
                <textarea class="form-control" rows="3" id="textMeta" name="textMeta" required="true" placeholder="ex.: Melhorar qualidade de vida"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="textRecompensa" class="">Recompensa</label><br/>
                <small>Escolha algo bom para você fazer quando concluir o projeto</small>
                <textarea class="form-control" rows="3" id="textRecompensa" name="textRecompensa" required="true" placeholder="ex.: Uma viagem à praia"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="inputCor" class="">Cor</label><br/>
                <small>Escolha uma cor para este projeto</small>
                <input type="text" class="form-control" id="inputCor" name="inputCor" required="true" data-toggle="modal" data-target="#modalCor">
            </div>
        </div>

        <legend>Níveis e Pontuações - Categoria Boa</legend>
        <div class="form-group">
            <div class="col-lg-12">
                <small>Crie níveis de hábitos bons e escolha uma pontuação para cada item</small>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-5">
                <label for="inputNivelBom" class="">Nível</label>
                <input type="text" class="form-control" id="inputNivelBom" placeholder="ex.: Ir de Bike para o trabalho">
            </div>
            <div class="col-lg-5">
                <label for="inputPontuacaoBoa" class="">Pontos</label>
                <input type="number" class="form-control" id="inputPontuacaoBoa" placeholder="ex.: 10" min="1"/>
            </div>
            <div class="col-lg-2">
                <label>&nbsp</label><br/>
                <button class="btn btn-default btn-block" type="button" id="incluirBom">
                    Incluir
                </button>
            </div>
            <hr/><br/>
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-danger hidden pontuacaoBom">
                        <strong>Opa!</strong> Preencha os dois campos
                    </div>
                </div>
            </div>
        </div>

        <legend>Níveis e Pontuações - Categoria Ruim</legend>
        <div class="form-group">
            <div class="col-lg-12">
                <small>Selecione níveis de hábitos ruins e escolha uma pontuação perdida para cada item</small>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-5">
                <label for="inputNivelRuim" class="">Nível</label>
                <input type="text" class="form-control" id="inputNivelRuim" placeholder="ex.: Comer fritura">
            </div>
            <div class="col-lg-5">
                <label for="inputPontuacaoRuim" class="">Pontos</label>
                <input type="number" class="form-control" id="inputPontuacaoRuim" placeholder="ex.: 10" max="-1"/>
            </div>
            <div class="col-lg-2">
                <label>&nbsp</label><br/>
                <button class="btn btn-default btn-block" type="button" id="incluirRuim">
                    Incluir
                </button>
            </div>
            <hr/><br/>
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-danger hidden pontuacaoRuim">
                        
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="col-lg-6">
            <table class="bonsHabitos table table-striped">
                <thead>
                    <tr>
                        <th colspan="3" class="text-center bg-success">Bons Hábitos</th>
                    </tr>
                    <tr>
                        <th class="col-lg-8">Nível</th>
                        <th class="col-lg-2">Pontuação</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

        <div class="col-lg-6">
            <table class="ruinsHabitos table table-striped">
                <thead>
                    <tr>
                        <th colspan="3" class="text-center bg-danger">Hábitos Ruins</th>
                    </tr>
                    <tr>
                        <th class="col-lg-8">Nível</th>
                        <th class="col-lg-2">Pontuação</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

        <input type="hidden" id="jsonNivelPontuacaoBom" name="jsonNivelPontuacaoBom"/>
        <input type="hidden" id="jsonNivelPontuacaoRuim" name="jsonNivelPontuacaoRuim"/>
        <input type="hidden" value="<?php print_r(date("Y-m-d h:i:s")); ?>" name="date"/>
        <div class="form-group">
            <div class="col-lg-12">
                <button type="submit" class="btn btn-success right">Salvar</button>
            </div>
        </div>
    </fieldset>
</form>


<!-- Modal -->
<div id="modalCor" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Escolha uma cor</h4>
            </div>
            <div class="modal-body">
                <ul class="list-inline list-cor">
                    <li class="turquoise"></li>
                    <li class="emerald"></li>
                    <li class="peter-river"></li>
                    <li class="amethyst"></li>
                    <li class="wet-asphalt"></li>
                    <li class="green-sea"></li>
                    <li class="nephritis"></li>
                    <li class="belize-hole"></li>
                    <li class="wisteria"></li>
                    <li class="midnight-blue"></li>
                    <li class="sun-flower"></li>
                    <li class="carrot"></li>
                    <li class="alizarin"></li>
                    <li class="concrete"></li>
                    <li class="orange"></li>
                    <li class="pumpkin"></li>
                    <li class="pomegranate"></li>
                    <li class="silver"></li>
                    <li class="asbestos"></li>

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $('.list-cor li').click(function () {
        $('#inputCor').val($(this).attr("class"));
        $('#modalCor').modal('hide');
    });
    
    
    $('#incluirBom').click(function () {
        if ($('#inputNivelBom').val() == "" || $('#inputPontuacaoBoa').val().length < 1) {
            $('.pontuacaoBom').removeClass('hidden');
        } else {
            $('.pontuacaoBom').addClass('hidden');
            html = '<tr><td data-nivel="' + $('#inputNivelBom').val() + '">' + $('#inputNivelBom').val() + '</td><td data-pontuacao="' + $('#inputPontuacaoBoa').val() + '">' + $('#inputPontuacaoBoa').val() + '</td><td><button type="button" class="btn btn-default btn-xs excluirBom" onclick="excluirBom(this)">Excluir</button></td></tr>';
            $('.bonsHabitos').find('tbody').prepend(html);
            // Atribuindo valor ao json
            criarJsonBom($('#inputNivelBom').val(), $('#inputPontuacaoBoa').val());
            $('#inputNivelBom').val("");
            $('#inputPontuacaoBoa').val("");
        }
    });

    function excluirBom(button) {
        var este = $(button).parent().parent();
        tirarJsonBom($(este).find(':nth-child(1)').html(), $(este).find(':nth-child(2)').html());
        este.remove();

    }

    var jsonNivelPontuacaoBom = "";

    function criarJsonBom(nivel, pontuacao) {
        linhajsonNivelPontuacao = '{"nivel":"' + nivel + '", "pontuacao" :"' + pontuacao + '"}';
        if (jsonNivelPontuacaoBom != "") {
            linhajsonNivelPontuacao = "," + linhajsonNivelPontuacao;
        }
        jsonNivelPontuacaoBom += linhajsonNivelPontuacao;
        var text = '[' + jsonNivelPontuacaoBom + ']';
        var obj = text;
        $('#jsonNivelPontuacaoBom').val(obj);
    }

    function tirarJsonBom(nivel, pontuacao) {
        tirarLinhajsonNivelPontuacao = '{"nivel":"' + nivel + '", "pontuacao" :"' + pontuacao + '"}';
        var first = jsonNivelPontuacaoBom.indexOf(tirarLinhajsonNivelPontuacao);
        var tirar = jsonNivelPontuacaoBom.substr(first, tirarLinhajsonNivelPontuacao.length);

        jsonNivelPontuacaoBom = jsonNivelPontuacaoBom.replace(tirar, "");
        jsonNivelPontuacaoBom = jsonNivelPontuacaoBom.replace(",,", ",");
        var text = '[' + jsonNivelPontuacaoBom + ']';

        jsonNivelPontuacaoBom = jsonNivelPontuacaoBom.replace(",]", "]");
        var obj = text;
        $('#jsonNivelPontuacaoBom').val(obj);

    }
    $('#incluirRuim').click(function () {
        
        if ($('#inputNivelRuim').val() == "" || $('#inputPontuacaoRuim').val().length < 1) {
            $('.pontuacaoRuim').removeClass('hidden');
            $('.pontuacaoRuim').html('<strong>Opa!</strong> Preencha os dois campos.');
        }  else if ($('#inputPontuacaoRuim').val() > 0) {
            $('.pontuacaoRuim').removeClass('hidden');
            $('.pontuacaoRuim').html('<strong>Opa!</strong> A pontuação deve ser Negativa.');
        }        
        else {
            $('.pontuacaoRuim').addClass('hidden');
            html = '<tr><td>' + $('#inputNivelRuim').val() + '</td><td>' + $('#inputPontuacaoRuim').val() + '</td><td><button type="button" class="btn btn-default btn-xs excluirRuim" onclick="excluirRuim(this)">Excluir</button></td></tr>';
            $('.ruinsHabitos').find('tbody').prepend(html);
            // Atribuindo valor ao json
            criarJsonRuim($('#inputNivelRuim').val(), $('#inputPontuacaoRuim').val());
            $('#inputNivelRuim').val("");
            $('#inputPontuacaoRuim').val("");
        }
    });
    var jsonNivelPontuacaoRuim = "";

    function excluirRuim(button) {
        var este = $(button).parent().parent();
        tirarJsonBom($(este).find(':nth-child(1)').html(), $(este).find(':nth-child(2)').html());
        este.remove();

    }

    function criarJsonRuim(nivel, pontuacao) {
        linhajsonNivelPontuacao = '{"nivel":"' + nivel + '", "pontuacao" :"' + pontuacao + '"}';
        if (jsonNivelPontuacaoRuim != "") {
            linhajsonNivelPontuacao = "," + linhajsonNivelPontuacao;
        }
        jsonNivelPontuacaoRuim += linhajsonNivelPontuacao;
        var text = '[' + jsonNivelPontuacaoRuim + ']';
        var obj = text;
        $('#jsonNivelPontuacaoRuim').val(obj);
    }

    function tirarJsonRuim(nivel, pontuacao) {
        tirarLinhajsonNivelPontuacao = '{"nivel":"' + nivel + '", "pontuacao" :"-' + pontuacao + '"}';
        var first = jsonNivelPontuacaoRuim.indexOf(tirarLinhajsonNivelPontuacao);
        var tirar = jsonNivelPontuacaoRuim.substr(first, tirarLinhajsonNivelPontuacao.length);

        jsonNivelPontuacaoRuim = jsonNivelPontuacaoRuim.replace(tirar, "");
        jsonNivelPontuacaoRuim = jsonNivelPontuacaoRuim.replace(",,", ",");
        var text = '[' + jsonNivelPontuacaoRuim + ']';

        jsonNivelPontuacaoRuim = jsonNivelPontuacaoRuim.replace(",]", "]");
        var obj = text;
        $('#jsonNivelPontuacaoRuim').val(obj);

    }
</script>
