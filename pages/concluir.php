<?php include('../sys/gerais.php'); ?>
<script>
    $(document).ready(function () {
        $('.voltar').removeClass('hidden');
    });
</script>

<?php if (incluirDados() == 0) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                Nenhum projeto selecionado!
            </div>
        </div>
    </div>
<?php } else { ?>
    <form class="form-horizontal" method="post" action="sys/concluir.php">
        <fieldset>
            <legend>
                <?php print_r(incluirDados()['tituloProjeto']); ?><br/>
                <small class="label label-info"><?php print_r(incluirDados()['metaProjeto']); ?></small><br/>
                <small class="label label-success"><?php print_r(incluirDados()['recompensaProjeto']); ?></small>
            </legend>
            
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Motivo Conclusão</label>
                    <textarea type="text" class="form-control" name="motivoConclusao" id="motivoConclusao" required="true"></textarea>
                </div>
            </div>
            
            <input type="hidden" id="idProjeto" name="idProjetoPontos" value="<?php print_r(incluirDados()['idProjeto']); ?>"/>
            <input type="hidden" value="<?php print_r(date("Y-m-d h:i:s")); ?>" name="date"/>
            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success right">Salvar</button>
                </div>
            </div>
        </fieldset>
    </form>

<?php } ?>

