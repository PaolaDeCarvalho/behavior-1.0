<?php

/* Global variables */
$SITE_FILE = 'http://localhost/behavior/';
$HOST_MYSQL = 'localhost';
$USER_MYSQL = 'root';
$PASS_MYSQL = '';
$INITIAL_DATABASE = 'Behavior';

$session = '';
$link = mysqli_connect($HOST_MYSQL, $USER_MYSQL, $PASS_MYSQL, $INITIAL_DATABASE);

function conectarBanco() {
    global $link;
    if (!$link) {
        die('Não foi possível conectar: ' . mysql_error());
    }
}

function desconectarBanco() {
    global $link;
    mysqli_close($link);
}

date_default_timezone_set('America/Sao_Paulo');
?>