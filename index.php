<!DOCTYPE html>
<html>
    <head>
        <title>Behavior</title>
        <meta charset="UTF-8">
        <meta name="description" content="Você pode mudar">
        <meta name="viewport" content="initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    </head>
    <?php
    include 'sys/config.php';
    session_start();
    unset($_SESSION);
    ?>
    <body class="midnight-blue">
        <main class="container">
            <div class="panel gray" id="pnlLogin">
                <div class="panel-heading transparent text-center">
                    <img src="img/logo.png" alt="Behavior" class="logo" />
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="sys/logar.php" method="post">
                        <?php if (isset($_GET["msg"]) != null) { ?>
                            <div class="alert alert-danger">
                                <?php print_r($_GET["msg"]); ?>
                            </div>
                        <?php } ?>
                        <div class="form-group has-feedback">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="login" placeholder="Login" />
                                <i class="glyphicon glyphicon-user form-control-feedback"></i>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-md-12">
                                <input type="password" class="form-control" name="senha" placeholder="Senha" />
                                <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn right rose c-white">
                                    <span class="glyphicon glyphicon-log-in"></span>
                                    &nbsp;Entrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </body>

</html>