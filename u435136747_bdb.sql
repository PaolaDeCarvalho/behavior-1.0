-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 11/07/2019 às 21:04
-- Versão do servidor: 10.2.23-MariaDB
-- Versão do PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u435136747_bdb`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Pontos`
--

CREATE TABLE `Pontos` (
  `idPontos` int(11) NOT NULL,
  `diaPontos` datetime DEFAULT NULL,
  `bomPontos` int(11) DEFAULT NULL,
  `ruimPontos` int(11) DEFAULT NULL,
  `totalPontos` int(11) DEFAULT NULL,
  `idProjetoPontos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Projeto`
--

CREATE TABLE `Projeto` (
  `idProjeto` int(11) NOT NULL,
  `tituloProjeto` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataCadastroProjeto` datetime DEFAULT NULL,
  `dataExclusaoProjeto` datetime DEFAULT NULL,
  `dataPrevistaFinalProjeto` datetime DEFAULT NULL,
  `categoriaRuimProjeto` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categoriaBoaProjeto` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaProjeto` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recompensaProjeto` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `concluidoProjeto` tinyint(1) DEFAULT NULL,
  `motivoConclusaoProjeto` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idUsuarioProjeto` int(11) DEFAULT NULL,
  `corProjeto` varchar(50) DEFAULT NULL,
  `nivelPontuacaoBomProjeto` varchar(50) DEFAULT NULL,
  `nivelPontuacaoRuimProjeto` varchar(50) DEFAULT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL,
  `nomeUsuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loginUsuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senhaUsuario` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataCadastroUsuario` datetime DEFAULT NULL,
  `dataExclusaoUsuario` datetime DEFAULT NULL,
  `fotoUsuario` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `Usuario`
--

INSERT INTO `Usuario` (`idUsuario`, `nomeUsuario`, `loginUsuario`, `senhaUsuario`, `dataCadastroUsuario`, `dataExclusaoUsuario`, `fotoUsuario`) VALUES
(1, 'Paola Carvalho', 'pcarvalho', '41ea2070c0bbd4dfae2191b02d29a0bc', '2018-10-06 02:34:16', NULL, 'http://paoladecarvalho.com.br/behavior/Files/Usuario/pcarvalho.jpg');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Pontos`
--
ALTER TABLE `Pontos`
  ADD PRIMARY KEY (`idPontos`);

--
-- Índices de tabela `Projeto`
--
ALTER TABLE `Projeto`
  ADD PRIMARY KEY (`idProjeto`);

--
-- Índices de tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Pontos`
--
ALTER TABLE `Pontos`
  MODIFY `idPontos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Projeto`
--
ALTER TABLE `Projeto`
  MODIFY `idProjeto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
