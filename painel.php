<?php
session_start();
include 'sys/config.php';
global $SITE_FILE;

if (!isset($_SESSION["nomeUsuario"])) {
    header("Location:" . $SITE_FILE . "index.php");
}
$_SESSION["nomeUsuario"];


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Behavior</title>
        <meta charset="UTF-8">
        <meta name="description" content="Você pode mudar">
        <meta name="viewport" content="initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php print_r($SITE_FILE); ?>css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php print_r($SITE_FILE); ?>css/bootstrap.css">
        <script src="<?php print_r($SITE_FILE); ?>js/jquery-3.1.1.js"></script>
        <script src="<?php print_r($SITE_FILE); ?>js/bootstrap.js"></script>
        <script src="<?php print_r($SITE_FILE); ?>js/modernizr-2.8.3.js"></script>
        <script src="<?php print_r($SITE_FILE); ?>js/script.js"></script>
        <script src="<?php print_r($SITE_FILE); ?>js/maskedinput.js"></script>
    </head>
    <body class="change-color">
        <header>
            <div class="wet-asphalt c-white">
                <div class="col-xs-3">
                    <img class="logo" src="<?php print_r($SITE_FILE); ?>img/logo-branco.png" alt="Behavior" />
                </div>
                <div class="col-xs-9">
                    <ul>
                        <li onclick="carregarPagina('novo', '')"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<span class="novo-mobile">Novo Projeto</span></li>
                    </ul>
                </div>
            </div>
            <div class="clouds">
                <div class="col-xs-4">
                    <div class="voltar hidden">
                        <button class="btn btn-success" onclick="carregarPagina('interna', '')">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                            &nbsp;&nbsp;Voltar
                        </button>
                    </div>
                    <div>

                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="right">
                        <label class="text-danger"><?php print_r($_SESSION["nomeUsuario"]); ?></label>
                        <img class="foto-perfil" src="<?php print_r($_SESSION["fotoUsuario"]); ?>" alt="Behavior" />
                        <button class="btn btn-default" onclick="carregarPagina('index', '')">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </div>
                </div>
            </div>
        </header>

        <main class="container">
            <section class="aviso">
                
            </section>
            <section class="page">
                
            </section>
            <script>
                $('[data-toggle="tooltip"]').tooltip(); 
                $('.page').load('pages/interna.php');
                var url = window.location.href;
                var indexOfP = url.indexOf("?");
                var indexOfM = url.indexOf("&msg=");
                if (indexOfP > 0) {
                    var html = '';
                    html = html + '<div class="row">';
                    html = html + '<div class="col-md-12">';
                    
                    if(url.substr(indexOfP + 3, 4) == 'true') {
                        html = html + '<div class="alert alert-success">';
                    }
                    else {
                        html = html + '<div class="alert alert-danger">';
                        
                    }
                    html = html + url.substr(indexOfM + 5);
                    html = html + '</div>';
                    html = html + '</div>';
                    html = html + '</div>';
                    
                    $('.aviso').append(html);
                }
                function carregarPagina(href, parameter) {
                    $('.aviso').children('.row').remove();
                    if (href == "index") {
                        console.log("olar");
                        window.location.href = "<?php print_r($SITE_FILE); ?>?msg=Logout Efetuado com Sucesso!";
                    } else {
                        var indexHref = url.indexOf("#");
                        var hrefbasico = url.substr(0, indexHref);
                        window.location.href = hrefbasico + "#" + href;
                        $('.page').load('pages/' + href + '.php' + parameter);



                    }
                }
            </script>
        </main>
    </body>

</html>